import Vue from 'vue'
import Router from 'vue-router'
import Task from 'components/Task/Tasks'
import Dashboard from 'components/Dashboard'


Vue.use(Router)

const router = new Router({
  routes:[
    {
      path: '/tasks',
      component: Task
    },
    {
      path:'/dashboard',
      component: Dashboard
    }
  ]
})

export default router
