import Vue from 'vue'
import EventBus from 'event-bus'
import App from 'components/App'
import router from 'router/index'

window.EventBus = EventBus

var vm = new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
